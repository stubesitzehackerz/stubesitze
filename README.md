## How to get this stuff running:

Download and install miniconda

Make sure to run:

- conda init
- conda creat -n scenesleuth

Install ffmpeg as described here: https://www.geeksforgeeks.org/how-to-install-ffmpeg-on-windows/

Navigate to the repo base, then backend folder, then run
- conda create -n scenesleuth
- conda activate scenesleuth
- pip install -r requirements.txt

Additionally, run the "install_gpu_version_of_pytorch.bat" file to get the GPU version of PyTorch (drastically reduces up processing time). Make sure you have a compatible GPU!

Then:
1. Fill out infile.out with the desired video metadata
2. Run "run_full_pipeline.bat". This script will run all steps of the pipeline, i.e.:
- Downloading the video
- Extracting audio from the video
- Extracting a transcript from the audio
- Creating chapters
- Summarizing the chapters
- Summarizing the entire video based on the chapter summaries

