# Use a pipeline as a high-level helper
from transformers import pipeline
import torch
import glob
import json
import time

batch_size = 1
max_chapter_length = 3072
num_concatenated_chapters = 30

print("Loading model...")
pipe = pipeline("summarization", model="knkarthick/MEETING_SUMMARY", device = 0 if torch.cuda.is_available() else -1)
print("Model loaded!\n")

for file in glob.glob("outfiles/json/*.json"):
    print(f"###### Processing {file}... #######")
    
    start_time = time.time()
    
    with open(file, "r", encoding='utf-8') as f:
        data = json.load(f)
    
    chapters = data[f"segment_batches_{num_concatenated_chapters}"]

    #put all chapter transcripts into one list for summarization
    outtext = []
    for i, chapter in enumerate(chapters):
        outtext.append(chapter["transcript"][:max_chapter_length])

    per_chapter_summary = pipe(outtext, max_length=120, min_length=30, batch_size=1)

    #write per-chapter summaries to new dictionary key
    data["per_chapter_summary"] = []
    for i, elem in enumerate(per_chapter_summary):
        data["per_chapter_summary"].append(dict(chapters[i]))
        data["per_chapter_summary"][i]["transcript"] = elem["summary_text"]

    # concatenate all chapter summaries into one string for overall summary
    concatenated_summaries = ""
    for elem in data["per_chapter_summary"]:
        concatenated_summaries += elem["transcript"] + " "

    #ensure we don't crash the program when the input is too long
    concatenated_summaries = concatenated_summaries[:max_chapter_length]

    summary_short = pipe(concatenated_summaries, max_length=100, min_length=100)
    summary_medium = pipe(concatenated_summaries, max_length=400, min_length=200)
    summary_long = pipe(concatenated_summaries, max_length=min(600, len(concatenated_summaries)), min_length=400)

    data["summary_short"] = summary_short[0]["summary_text"]
    data["summary_medium"] = summary_medium[0]["summary_text"]
    data["summary_long"] = summary_long[0]["summary_text"]

    #update the json file to include the per-chapter summaries and the overall summary
    with open(file, "w", encoding='utf-8') as f:
        f.write(json.dumps(data, indent=4, ensure_ascii=False))

    print(f"###### Done processing {file}! Took {time.time()-start_time:.1f}s #######\n")
