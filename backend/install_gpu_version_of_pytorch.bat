@echo off
pip uninstall torch -y
pip cache purge
pip install torch -f https://download.pytorch.org/whl/torch_stable.html
