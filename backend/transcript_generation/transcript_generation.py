import os
import requests
from moviepy.editor import *
import whisper_timestamped as whisper
import json  

def download_video(url, output_filename="temp_video.mp4"):
    """
    Download video from a given direct URL.
    
    Parameters:
    - url: Direct URL of the video to download.
    - output_filename: Name for the downloaded file.
    
    Returns:
    - File path of the downloaded video.
    """
    response = requests.get(url, stream=True)
    response.raise_for_status()
    
    with open(output_filename, 'wb') as file:
        for chunk in response.iter_content(chunk_size=8192):
            file.write(chunk)
    
    return output_filename

def extract_audio_from_video(input_video, output_audio):
    """
    Extract audio from video and save it to the specified output file.
    
    Parameters:
    - input_video: Path to the input video file.
    - output_audio: Path to save the extracted audio (e.g., in MP3 format).
    """
    video = VideoFileClip(input_video)
    audio = video.audio
    audio.write_audiofile(output_audio)
    audio.close()
    video.close()

def extract_transcript(filename, lan):

    audio = whisper.load_audio(filename)
    model = whisper.load_model("base")

    language = "en" #"en" for english

    # transcribe audio
    result = whisper.transcribe(model, audio, language=lan)
    return result

def seconds_to_hms(seconds):
    hours, remainder = divmod(seconds, 3600)
    minutes, seconds = divmod(remainder, 60)
    return "{:02}:{:02}:{:02}".format(int(hours), int(minutes), int(seconds))

def seconds_to_mins_secs(seconds_float):
    # Get total seconds as an integer
    total_seconds = int(seconds_float)
    
    # Calculate minutes and seconds
    mins, secs = divmod(total_seconds, 60)
    
    # Return formatted string
    return f"{mins}m{secs}s"

def format_and_write(transcript, lecture_id, lecture_title, department, password_protected, episode, episode_title, episode_date, vvz_url, video_url, language):

  # put everything into nice format
    output = {"metadata": {"lecture_id": lecture_id,
                          "lecture_title": lecture_title,
                          "department": department,
                          "password_protected": password_protected,
                          "episode": episode,
                          "episode_title": episode_title,
                          "episode_date": episode_date,
                          "vvz_url": vvz_url,
                          "video_url": video_url,
                          "language": language},
              "segments": []}

    outtext = ""
    plaintext = ""

    for i, segment in enumerate(transcript['segments']):
      short_segment = {}
      short_segment["start"] = segment["start"]
      short_segment["start_formatted"] = seconds_to_mins_secs(int(segment["start"]))
      short_segment["end"] = segment["end"]
      short_segment["end_formatted"] = seconds_to_mins_secs(int(segment["end"]))
      short_segment["transcript"] = segment['text'].strip()
      output["segments"].append(short_segment)

      outtext += short_segment["start_formatted"] + "\n"
      outtext += short_segment["transcript"] + "\n\n"
      plaintext += short_segment["transcript"] + " "
      

    # save to json file
    with open(f"outfiles/json/outtext_json_{lecture_id}_{episode}.json", "w", encoding='utf-8') as outfile:
        outfile.write(json.dumps(output, indent=4, ensure_ascii=False))
        
    with open(f"outfiles/plaintext_with_timestamps/outtext_with_timestamps_{lecture_id}_{episode}.txt", "w", encoding='utf-8') as f:
        f.write(outtext)

    with open(f"outfiles/plaintext/outtext_plain_{lecture_id}_{episode}.txt", "w", encoding='utf-8') as f:
        f.write(plaintext)

    print("Wrote transcript")

if __name__ == "__main__":
  # go over infile, download video for every request and transcribe
  with open("infile.json") as f:
      infile = json.load(f)

  print("Loaded infile!")

  os.makedirs("outfiles/json/", exist_ok=True)
  os.makedirs("outfiles/plaintext_with_timestamps/", exist_ok=True)
  os.makedirs("outfiles/plaintext/", exist_ok=True)

  for lecture in infile["lectures_to_transcribe"]:
      for elem in lecture["videos"]:
          
          #download the video
          print("Downloading video " + elem["video_url"] + "...")
          download_video(elem["video_url"], output_filename = "temp_video.mp4")
          print("Downloaded video")
          
          #extract only audio from video
          print("Extracting audio...")
          extract_audio_from_video("temp_video.mp4", "temp_audio.mp3")
          print("Extracted audio!")
          
          #
          print("Extracting transcript...")
          transcript = extract_transcript("temp_audio.mp3", lecture["language"])
          print("Extracted transcript!")
          
          print("Formatting and writing...")
          format_and_write(transcript, lecture["lecture_id"], lecture["lecture_title"], lecture["department"], lecture["password_protected"], elem["episode"], elem["episode_title"], elem["episode_date"], elem["vvz_url"], elem["video_url"], lecture["language"])
          print("Formatted and wrote!")

          os.remove("temp_video.mp4")
          os.remove("temp_audio.mp3")

          print("Deleted temp files!")

print("Finished extracting from all videos!")