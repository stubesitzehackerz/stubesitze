import json
import os

num_concatenated_chapters = 30

def add_key_value_json(new_data, keyname, filename):
    print(os.path.join(path_to_json, filename))
    with open(os.path.join(path_to_json, filename), 'r+') as file:
          # First we load existing data into a dict.
        file_data = json.load(file)
        # Join new_data with file_data inside emp_details
        file_data[keyname] = new_data
        # Sets file's current position at offset.
        file.seek(0)
        # convert back to json.
        json.dump(file_data, file, indent = 4)

#path_to_json = os.path.join(os.getcwd(), 'stubesitze', 'backend', 'transcript_generation', 'outfiles', 'json_copy')
path_to_json = "outfiles/json/"
json_files = [pos_json for pos_json in os.listdir(path_to_json) if pos_json.endswith('.json')]

for transcript_file in json_files:
    current_file = open(os.path.join(path_to_json, transcript_file))
    data = json.load(current_file)
    list_of_batches = []
    list_of_sentences = []
    list_of_timestamps = []
    for json_sentence in data['segments']:
        if len(list_of_sentences) >= num_concatenated_chapters:
            list_of_batches.append(list_of_sentences)
            list_of_sentences = []
        if len(list_of_sentences) == 0:
            list_of_timestamps.append(json_sentence['start'])
            list_of_timestamps.append(json_sentence['start_formatted'])
        list_of_sentences.append(json_sentence['transcript'])
        if len(list_of_sentences) == num_concatenated_chapters:
            list_of_timestamps.append(json_sentence['end'])
            list_of_timestamps.append(json_sentence['end_formatted'])
    current_file.close()


    for i in range (len(list_of_batches)):
        list_of_batches[i] = ' '.join(map(str, list_of_batches[i]))

    list_of_json_batches = []
    for k in range (len(list_of_batches)):
        list_of_json_batches.append({"start": list_of_timestamps[4*k],
                                "start_formatted": list_of_timestamps[4*k + 1],
                                "end": list_of_timestamps[4*k + 2],
                                "end_formatted": list_of_timestamps[4*k + 3],
                                "transcript": list_of_batches[k]
                                })

    #print(list_of_json_batches)

    #segments_batches = {"segment_batches " + str(num_concatenated_chapters): list_of_json_batches}

    #y = json.load(segments_batches)

    add_key_value_json(list_of_json_batches, "segment_batches_" + str(num_concatenated_chapters), transcript_file)

    #print(list_of_json_batches)
    #print(list_of_timestamps)
    #print(list_of_batches)
    #print(list_of_sentences)
    #print(path_to_json)
    #print(json_files)

