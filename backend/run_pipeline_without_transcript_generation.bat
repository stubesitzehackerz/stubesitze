@echo off

echo "Activating conda environment..."
call conda activate scenesleuth

echo "Running transcript_transformation.py..."
call python transcript_generation/transcript_transformation.py
echo "Running summarizer.py..."
call python summarization/summarizer.py
echo "### Pipeline finished running! ###"